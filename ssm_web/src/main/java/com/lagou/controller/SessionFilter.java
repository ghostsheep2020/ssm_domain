package com.lagou.controller;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * @author Administrator
 * @date 2020/11/2 15:55
 * @description
 */
public class SessionFilter implements Filter {
    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) resp;
        String jsessionId = CookieUtils.getCookieValue(request, "JSESSIONID");

//将session获取出来

        HttpSession session = request.getSession();

//System.out.println("-----jsessionId------->>"+jsessionId);

        CookieUtils.setCookie(request, response, "JSESSIONID", jsessionId);
        chain.doFilter(req, resp);
    }

    public void init(FilterConfig config) throws ServletException {

    }

}
